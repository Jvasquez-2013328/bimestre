﻿using System.Web;
using System.Web.Mvc;

namespace CineKinal_2013328
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
