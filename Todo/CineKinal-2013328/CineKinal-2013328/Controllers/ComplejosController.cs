﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CineKinal_2013328.Models;

namespace CineKinal_2013328.Controllers
{
    public class ComplejosController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: /Complejos/
        public ActionResult Index()
        {
            return View(db.Complejoes.ToList());
        }

        // GET: /Complejos/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Complejo complejo = db.Complejoes.Find(id);
            if (complejo == null)
            {
                return HttpNotFound();
            }
            return View(complejo);
        }

        // GET: /Complejos/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: /Complejos/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="ID,NombreComplejo,Direccion,Ciudad")] Complejo complejo)
        {
            if (ModelState.IsValid)
            {
                db.Complejoes.Add(complejo);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(complejo);
        }

        // GET: /Complejos/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Complejo complejo = db.Complejoes.Find(id);
            if (complejo == null)
            {
                return HttpNotFound();
            }
            return View(complejo);
        }

        // POST: /Complejos/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="ID,NombreComplejo,Direccion,Ciudad")] Complejo complejo)
        {
            if (ModelState.IsValid)
            {
                db.Entry(complejo).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(complejo);
        }

        // GET: /Complejos/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Complejo complejo = db.Complejoes.Find(id);
            if (complejo == null)
            {
                return HttpNotFound();
            }
            return View(complejo);
        }

        // POST: /Complejos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Complejo complejo = db.Complejoes.Find(id);
            db.Complejoes.Remove(complejo);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
