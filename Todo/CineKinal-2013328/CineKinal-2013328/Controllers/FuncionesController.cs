﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CineKinal_2013328.Models;

namespace CineKinal_2013328.Controllers
{
    public class FuncionesController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: /Funciones/
        public ActionResult Index()
        {
            var funcions = db.Funcions.Include(f => f.Pelicula).Include(f => f.Sala);
            return View(funcions.ToList());
        }

        // GET: /Funciones/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Funcion funcion = db.Funcions.Find(id);
            if (funcion == null)
            {
                return HttpNotFound();
            }
            return View(funcion);
        }

        // GET: /Funciones/Create
        public ActionResult Create()
        {
            ViewBag.PeliculaId = new SelectList(db.Peliculas, "ID", "Titulo");
            ViewBag.SalaId = new SelectList(db.Salas, "ID", "NumeroSala");
            return View();
        }

        // POST: /Funciones/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="ID,Precio,Clasificacion,PeliculaId,SalaId")] Funcion funcion)
        {
            if (ModelState.IsValid)
            {
                db.Funcions.Add(funcion);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.PeliculaId = new SelectList(db.Peliculas, "ID", "Titulo", funcion.PeliculaId);
            ViewBag.SalaId = new SelectList(db.Salas, "ID", "NumeroSala", funcion.SalaId);
            return View(funcion);
        }

        // GET: /Funciones/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Funcion funcion = db.Funcions.Find(id);
            if (funcion == null)
            {
                return HttpNotFound();
            }
            ViewBag.PeliculaId = new SelectList(db.Peliculas, "ID", "Titulo", funcion.PeliculaId);
            ViewBag.SalaId = new SelectList(db.Salas, "ID", "NumeroSala", funcion.SalaId);
            return View(funcion);
        }

        // POST: /Funciones/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="ID,Precio,Clasificacion,PeliculaId,SalaId")] Funcion funcion)
        {
            if (ModelState.IsValid)
            {
                db.Entry(funcion).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.PeliculaId = new SelectList(db.Peliculas, "ID", "Titulo", funcion.PeliculaId);
            ViewBag.SalaId = new SelectList(db.Salas, "ID", "NumeroSala", funcion.SalaId);
            return View(funcion);
        }

        // GET: /Funciones/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Funcion funcion = db.Funcions.Find(id);
            if (funcion == null)
            {
                return HttpNotFound();
            }
            return View(funcion);
        }

        // POST: /Funciones/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Funcion funcion = db.Funcions.Find(id);
            db.Funcions.Remove(funcion);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
