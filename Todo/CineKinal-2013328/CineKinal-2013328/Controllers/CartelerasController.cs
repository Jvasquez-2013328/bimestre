﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CineKinal_2013328.Models;

namespace CineKinal_2013328.Controllers
{
    public class CartelerasController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: /Carteleras/
        public ActionResult Index()
        {
            var carteleras = db.Carteleras.Include(c => c.Pelicula);
            return View(carteleras.ToList());
        }

        // GET: /Carteleras/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Cartelera cartelera = db.Carteleras.Find(id);
            if (cartelera == null)
            {
                return HttpNotFound();
            }
            return View(cartelera);
        }

        // GET: /Carteleras/Create
        public ActionResult Create()
        {
            ViewBag.PeliculaId = new SelectList(db.Peliculas, "ID", "Titulo");
            return View();
        }

        // POST: /Carteleras/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="ID,Descripcion,Imagen,Fecha,HoraInicio,HoraFin,PeliculaId")] Cartelera cartelera)
        {
            if (ModelState.IsValid)
            {
                db.Carteleras.Add(cartelera);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.PeliculaId = new SelectList(db.Peliculas, "ID", "Titulo", cartelera.PeliculaId);
            return View(cartelera);
        }

        // GET: /Carteleras/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Cartelera cartelera = db.Carteleras.Find(id);
            if (cartelera == null)
            {
                return HttpNotFound();
            }
            ViewBag.PeliculaId = new SelectList(db.Peliculas, "ID", "Titulo", cartelera.PeliculaId);
            return View(cartelera);
        }

        // POST: /Carteleras/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="ID,Descripcion,Imagen,Fecha,HoraInicio,HoraFin,PeliculaId")] Cartelera cartelera)
        {
            if (ModelState.IsValid)
            {
                db.Entry(cartelera).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.PeliculaId = new SelectList(db.Peliculas, "ID", "Titulo", cartelera.PeliculaId);
            return View(cartelera);
        }

        // GET: /Carteleras/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Cartelera cartelera = db.Carteleras.Find(id);
            if (cartelera == null)
            {
                return HttpNotFound();
            }
            return View(cartelera);
        }

        // POST: /Carteleras/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Cartelera cartelera = db.Carteleras.Find(id);
            db.Carteleras.Remove(cartelera);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
