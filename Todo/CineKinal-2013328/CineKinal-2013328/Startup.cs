﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(CineKinal_2013328.Startup))]
namespace CineKinal_2013328
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
