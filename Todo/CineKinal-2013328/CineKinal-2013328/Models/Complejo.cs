﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace CineKinal_2013328.Models
{
    public class Complejo
    {
        public int ID { get; set; }
        [Display(Name = "Nombre del Complejo")]
        public string NombreComplejo { get; set; }
        
        public string Direccion { get; set; }
        
        public string Ciudad { get; set; }
    }
}