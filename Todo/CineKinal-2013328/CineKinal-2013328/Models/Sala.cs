﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace CineKinal_2013328.Models
{
    public class Sala
    {
        public int ID { get; set; }
        [Display(Name = "Numero de sala")]
        public string NumeroSala { get; set; }
        [Display(Name = "Tipo 2D o 3D")]
        public string Tipo { get; set; }

        //Complejo 
        public int ComplejoId { get; set; }
        public Complejo Complejo { get; set; }
    }
}