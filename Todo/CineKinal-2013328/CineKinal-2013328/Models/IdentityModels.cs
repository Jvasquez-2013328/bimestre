﻿using Microsoft.AspNet.Identity.EntityFramework;

namespace CineKinal_2013328.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection")
        {
        }

        public System.Data.Entity.DbSet<CineKinal_2013328.Models.Pelicula> Peliculas { get; set; }

        public System.Data.Entity.DbSet<CineKinal_2013328.Models.Cartelera> Carteleras { get; set; }

        public System.Data.Entity.DbSet<CineKinal_2013328.Models.Complejo> Complejoes { get; set; }

        public System.Data.Entity.DbSet<CineKinal_2013328.Models.Funcion> Funcions { get; set; }

        public System.Data.Entity.DbSet<CineKinal_2013328.Models.Sala> Salas { get; set; }
    }
}