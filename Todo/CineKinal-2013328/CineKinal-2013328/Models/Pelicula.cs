﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace CineKinal_2013328.Models
{
    public class Pelicula
    {
        public int ID { get; set; }
        [Display(Name = "Titulo")]
        public string Titulo { get; set; }
        [Display(Name = "Genero")]
        public string Genero { get; set; }
        [Display(Name = "Precio")]
        public decimal Precio { get; set; }
    }
}