﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace CineKinal_2013328.Models
{
    public class Funcion
    {
        public int ID { get; set; }
        
        public decimal Precio { get; set; }
        
        public string Clasificacion { get; set; }

        //Pelicula
        public int PeliculaId { get; set; }
        public Pelicula Pelicula { get; set; }

        //Sala
        public int SalaId { get; set; }
        public Sala Sala { get; set; }
    }
}