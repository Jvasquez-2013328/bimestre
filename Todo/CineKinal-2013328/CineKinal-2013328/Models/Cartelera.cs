﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace CineKinal_2013328.Models
{
    public class Cartelera
    {
        public int ID { get; set; }
        [Display(Name = "Fecha")]
        public string Descripcion { get; set; }
        public string Imagen { get; set; }
        public DateTime Fecha { get; set; }
        [Display(Name = "Hora de inicio")]
        public string HoraInicio { get; set; }
        [Display(Name = "Hora Fin")]
        public string HoraFin { get; set; }

        //LLave foranea Pelicula
        public int PeliculaId { get; set; }
        public Pelicula Pelicula { get; set; }
  
    }
}