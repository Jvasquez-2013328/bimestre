﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Banco.Models;

namespace Banco.Controllers
{
    public class MovimientoesController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Movimientoes
        public ActionResult Index()
        {
            var movimientoes = db.Movimientoes.Include(m => m.TipoMovimiento);
            return View(movimientoes.ToList());
        }

        // GET: Movimientoes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Movimiento movimiento = db.Movimientoes.Find(id);
            if (movimiento == null)
            {
                return HttpNotFound();
            }
            return View(movimiento);
        }

        // GET: Movimientoes/Create
        public ActionResult Create()
        {
            ViewBag.TipoMovimientoId = new SelectList(db.TipoMovimientoes, "Id", "Tipo");
            return View();
        }

        // POST: Movimientoes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,CantidadConsumida,CantidadRetirada,FechaRealizado,Hora,TipoMovimientoId")] Movimiento movimiento)
        {
            if (ModelState.IsValid)
            {
                db.Movimientoes.Add(movimiento);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.TipoMovimientoId = new SelectList(db.TipoMovimientoes, "Id", "Tipo", movimiento.TipoMovimientoId);
            return View(movimiento);
        }

        // GET: Movimientoes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Movimiento movimiento = db.Movimientoes.Find(id);
            if (movimiento == null)
            {
                return HttpNotFound();
            }
            ViewBag.TipoMovimientoId = new SelectList(db.TipoMovimientoes, "Id", "Tipo", movimiento.TipoMovimientoId);
            return View(movimiento);
        }

        // POST: Movimientoes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,CantidadConsumida,CantidadRetirada,FechaRealizado,Hora,TipoMovimientoId")] Movimiento movimiento)
        {
            if (ModelState.IsValid)
            {
                db.Entry(movimiento).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.TipoMovimientoId = new SelectList(db.TipoMovimientoes, "Id", "Tipo", movimiento.TipoMovimientoId);
            return View(movimiento);
        }

        // GET: Movimientoes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Movimiento movimiento = db.Movimientoes.Find(id);
            if (movimiento == null)
            {
                return HttpNotFound();
            }
            return View(movimiento);
        }

        // POST: Movimientoes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Movimiento movimiento = db.Movimientoes.Find(id);
            db.Movimientoes.Remove(movimiento);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
