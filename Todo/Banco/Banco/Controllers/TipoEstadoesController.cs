﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Banco.Models;

namespace Banco.Controllers
{
    public class TipoEstadoesController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: TipoEstadoes
        public ActionResult Index()
        {
            return View(db.TipoEstadoes.ToList());
        }

        // GET: TipoEstadoes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TipoEstado tipoEstado = db.TipoEstadoes.Find(id);
            if (tipoEstado == null)
            {
                return HttpNotFound();
            }
            return View(tipoEstado);
        }

        // GET: TipoEstadoes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: TipoEstadoes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Tipo")] TipoEstado tipoEstado)
        {
            if (ModelState.IsValid)
            {
                db.TipoEstadoes.Add(tipoEstado);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(tipoEstado);
        }

        // GET: TipoEstadoes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TipoEstado tipoEstado = db.TipoEstadoes.Find(id);
            if (tipoEstado == null)
            {
                return HttpNotFound();
            }
            return View(tipoEstado);
        }

        // POST: TipoEstadoes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Tipo")] TipoEstado tipoEstado)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tipoEstado).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tipoEstado);
        }

        // GET: TipoEstadoes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TipoEstado tipoEstado = db.TipoEstadoes.Find(id);
            if (tipoEstado == null)
            {
                return HttpNotFound();
            }
            return View(tipoEstado);
        }

        // POST: TipoEstadoes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            TipoEstado tipoEstado = db.TipoEstadoes.Find(id);
            db.TipoEstadoes.Remove(tipoEstado);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
