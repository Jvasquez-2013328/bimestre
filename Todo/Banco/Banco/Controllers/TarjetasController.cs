﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Banco.Models;

namespace Banco.Controllers
{
    public class TarjetasController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: /Tarjetas/
        public ActionResult Index()
        {
            var tarjetas = db.Tarjetas.Include(t => t.Estado).Include(t => t.TipoTarjeta);
            return View(tarjetas.ToList());
        }

        // GET: /Tarjetas/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Tarjeta tarjeta = db.Tarjetas.Find(id);
            if (tarjeta == null)
            {
                return HttpNotFound();
            }
            return View(tarjeta);
        }

        // GET: /Tarjetas/Create
        public ActionResult Create()
        {
            ViewBag.EstadoId = new SelectList(db.Estadoes, "Id", "Motivo");
            ViewBag.TipoTarjetaId = new SelectList(db.TipoTarjetas, "Id", "Tipo");
            return View();
        }

        // POST: /Tarjetas/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="NumeroTarjeta,PinAcceso,NumeroSecreeto,FechaCreacion,FechaValidez,Limite,FechaPago,FechaCorte,TipoTarjetaId,EstadoId")] Tarjeta tarjeta)
        {
            if (ModelState.IsValid)
            {
                db.Tarjetas.Add(tarjeta);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.EstadoId = new SelectList(db.Estadoes, "Id", "Motivo", tarjeta.EstadoId);
            ViewBag.TipoTarjetaId = new SelectList(db.TipoTarjetas, "Id", "Tipo", tarjeta.TipoTarjetaId);
            return View(tarjeta);
        }

        // GET: /Tarjetas/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Tarjeta tarjeta = db.Tarjetas.Find(id);
            if (tarjeta == null)
            {
                return HttpNotFound();
            }
            ViewBag.EstadoId = new SelectList(db.Estadoes, "Id", "Motivo", tarjeta.EstadoId);
            ViewBag.TipoTarjetaId = new SelectList(db.TipoTarjetas, "Id", "Tipo", tarjeta.TipoTarjetaId);
            return View(tarjeta);
        }

        // POST: /Tarjetas/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="NumeroTarjeta,PinAcceso,NumeroSecreeto,FechaCreacion,FechaValidez,Limite,FechaPago,FechaCorte,TipoTarjetaId,EstadoId")] Tarjeta tarjeta)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tarjeta).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.EstadoId = new SelectList(db.Estadoes, "Id", "Motivo", tarjeta.EstadoId);
            ViewBag.TipoTarjetaId = new SelectList(db.TipoTarjetas, "Id", "Tipo", tarjeta.TipoTarjetaId);
            return View(tarjeta);
        }

        // GET: /Tarjetas/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Tarjeta tarjeta = db.Tarjetas.Find(id);
            if (tarjeta == null)
            {
                return HttpNotFound();
            }
            return View(tarjeta);
        }

        // POST: /Tarjetas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            Tarjeta tarjeta = db.Tarjetas.Find(id);
            db.Tarjetas.Remove(tarjeta);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
