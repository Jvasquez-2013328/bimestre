﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Banco.Models
{
    public class TipoEstado
    {
        [Key]
        public int Id { get; set; }
        [Display (Name = "Estado de la Tarjeta")]
        public string Tipo { get; set; }
    }
}