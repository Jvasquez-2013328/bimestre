﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Banco.Models
{
    public class Movimiento
    {
        public int Id { get; set;}
        [Display(Name = "Cantidad Consumida")]
        public decimal CantidadConsumida { get; set;}
        [Display(Name = "Cantidad Retirada")]
	    public decimal CantidadRetirada { get; set;}
        [Display(Name = "Fecha Realizado")]
	    public DateTime FechaRealizado{ get; set;}
	    public  TimeSpan Hora { get; set;}

        //Tipo Movimineto
        [Display(Name = "Tipo de Movimiento")]
        public int TipoMovimientoId { get; set; }
        public TipoMovimiento TipoMovimiento { get; set; }
    }
}