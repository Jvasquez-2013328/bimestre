﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Banco.Models
{
    public class Cuenta
    {
        public int id { get; set; }
        [Display(Name = "Numero de Cuenta")]
        public long NumeroCuenta { get; set; }
        public decimal Saldo { get; set; }

        //Persona
        [Display(Name = "Persona")]
        public int PersonaId { get; set; }
        public Persona Persona { get; set; }
        //Tipo Cuenta
        [Display(Name = "Tipo De Cuenta")]
        public int TipoCuentaId { get; set; }
        public TipoCuenta TipoCuenta { get; set; }
        

    }
}