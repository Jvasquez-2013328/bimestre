﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;


namespace Banco.Models
{
    public class Persona
    {
        [Key]
        public int Id { get; set;}
	    public string Nombre { get; set;}
	    public string Apellido { get; set;}
	    public int Edad { get; set;}
	    public string Correo { get; set;}
	    public int Telefono { get; set;}
	    public string Direccion { get; set;}
        [Display(Name = "Fecha de Nacimiento")]
        public DateTime FechaNacimiento { get; set; }
    }
}