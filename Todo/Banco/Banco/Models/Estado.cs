﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Banco.Models
{
    public class Estado
    {
        public int Id { get; set; }
	    public string Motivo { get; set; }
        public DateTime Fecha { get; set; }


        //Tipo de Estado
        [Display(Name = "Tipo de Estado")]
        public int TipoEstadoId { get; set; }
        public TipoEstado TipoEstado { get; set; }
     }
}