﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Banco.Models
{
    public class TipoCuenta
    {
        [Key]
        public int Id { get; set; }
        [Display (Name = "Tipo de Cuenta")]
        public string Tipo { get; set; }
    }
}