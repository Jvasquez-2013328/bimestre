﻿using Microsoft.AspNet.Identity.EntityFramework;

namespace Banco.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection")
        {
        }

        public System.Data.Entity.DbSet<Banco.Models.Persona> Personas { get; set; }

        public System.Data.Entity.DbSet<Banco.Models.Cuenta> Cuentas { get; set; }

        public System.Data.Entity.DbSet<Banco.Models.TipoCuenta> TipoCuentas { get; set; }

        public System.Data.Entity.DbSet<Banco.Models.TipoTarjeta> TipoTarjetas { get; set; }

        public System.Data.Entity.DbSet<Banco.Models.Movimiento> Movimientoes { get; set; }

        public System.Data.Entity.DbSet<Banco.Models.TipoMovimiento> TipoMovimientoes { get; set; }

        public System.Data.Entity.DbSet<Banco.Models.Estado> Estadoes { get; set; }

        public System.Data.Entity.DbSet<Banco.Models.TipoEstado> TipoEstadoes { get; set; }

        public System.Data.Entity.DbSet<Banco.Models.Prestamo> Prestamoes { get; set; }

        public System.Data.Entity.DbSet<Banco.Models.Tarjeta> Tarjetas { get; set; }
    }
}