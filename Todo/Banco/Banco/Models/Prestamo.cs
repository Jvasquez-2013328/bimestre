﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
namespace Banco.Models
{
    public class Prestamo
    {
        public int Id { get; set; }
        [Display(Name = "Cantidad de Meses")]
        public int CantidadMeses { get; set; }
	    public decimal Monto  { get; set; }
	    public int Plazo { get; set; }
        [Display(Name ="Fecha Inicial")]
	    public DateTime FechaInicial { get; set; }
        [Display(Name = "Fecha de Pago")]
        public DateTime FechaPago { get; set; }

        //cUENTA

        public int CuentaId { get; set; }
        public Cuenta Cuenta { get; set; }


    }
}