﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Banco.Models
{
    public class Tarjeta
    {
        [Key]
        [Display(Name = "Numero de Tarjeta")]
        public long NumeroTarjeta { get; set; }
        [Display(Name = "Pin de Acceso")]
	    public int PinAcceso { get; set; }
        [Display(Name = "Numero Secreto")]
	    public int NumeroSecreeto { get; set; }
        [Display (Name ="Fecha de Creacion")]
	    public DateTime FechaCreacion { get; set; }
        [Display(Name = "Fecha de Validez")]
        public DateTime FechaValidez { get; set; }
	    public decimal Limite { get; set; }
        [Display(Name = "Fecha de Pago")]
        public DateTime FechaPago { get; set; }
        [Display(Name = "Fecha de Corte")]
        public DateTime FechaCorte { get; set; }

        //Tipo Tarjeta
        [Display(Name = "Tipo de Tarjeta")]
        public int TipoTarjetaId { get; set; }
        public TipoTarjeta TipoTarjeta { get; set; }

        //Estado
        public int EstadoId { get; set; }
        public Estado Estado { get; set; }
    }
}